#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Docstring goes here"""

# ----------------------------------------------------------------------------------------------------------------------
# Importing packages
# ----------------------------------------------------------------------------------------------------------------------
from collections import Counter, defaultdict
import sys, os, glob
import time, datetime
import re
import itertools
from pprint import pprint
import itertools

# ----------------------------------------------------------------------------------------------------------------------
# Functions
# ----------------------------------------------------------------------------------------------------------------------

# ----------------------------------------------------------------------------------------------------------------------
# Variables
# ----------------------------------------------------------------------------------------------------------------------
section_count = itertools.count(0)


# ----------------------------------------------------------------------------------------------------------------------
# Script
# ----------------------------------------------------------------------------------------------------------------------
if __name__ == '__main__' and section_count.__next__() < 1:
    import datetime
    from django.utils import timezone
    from polls.models import Question
     # create a Question instance with pub_date 30 days in the future
    future_question = Question(pub_date=timezone.now() + datetime.timedelta(days=30))
    print(future_question)
     # was it published recently?
    print(future_question.was_published_recently())



if __name__ == '__main__' and section_count.__next__() < 1:
    from polls import Question, Choice

    print(Choice)
    print(Question)

if __name__ == '__main__' and section_count.__next__() < 1:
    path = os.path.abspath(__file__)
    for i in range(9):
        print(path)
        path = os.path.dirname(path)
